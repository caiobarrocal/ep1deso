/* MAC0422 - Sistemas Operacionais - Exercício Programa 1
   Caio Barrocal Fernandes Nº 8941130
   Pedro Ivo Siqueira Nepomuceno Nº 8941321
   Bacharelado em Cîência da Computação */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <readline/history.h>

#define FIRST_BUFFER 1024

/*Devolve processo e os argumentos
em tokens separados*/
void interpretInput (char *raw_command,char **table) {

  int table_index;
  char *word;  

  table_index=0;
  word = strtok(raw_command, " \n");

  while (word != NULL) {

    table[table_index] = word;
    table_index++;

    word = strtok(NULL, " \n");
  }
  table[table_index] = NULL; //Garante que a chamada de sistema não possua 'argumentos brancos'
}


int main() {

  /*alocando para obter diretório atual */
  char current_dir[FIRST_BUFFER], msg[FIRST_BUFFER], *command,**command_table;
  int status;
  int num_mode, UID,i;
  HIST_ENTRY **historico;
  HISTORY_STATE *state;

  command_table = malloc(sizeof(char*)*FIRST_BUFFER);
  command=NULL;

  using_history(); //Inicializando variaveis da biblioteca

  while(1) {

    if (getcwd(current_dir, sizeof(current_dir)) != NULL){
        strcpy(msg,"(");
        strcat(msg,current_dir);
        strcat(msg,"): \0");
        if(command!=NULL) free(command); //desaloca comando anterior. 
        command = readline(msg); //Readline aloca espaço
    }    

    if(command) add_history(command);
    /*chama função para interpretar a entrada do usuário */
    interpretInput(command,command_table);


    if (strcmp(command_table[0],"chmod") == 0) {

      /* recebemos um chmod e vamos tratar os argumentos */
      num_mode = strtol(command_table[1], 0, 8);
      chmod(command_table[2], num_mode);

    } else if (strcmp(command_table[0],"id") == 0) {

      if (strcmp(command_table[1],"-u") == 0) {
        UID = getuid();
        printf("%d\n", UID);
      }

    } 
    else if (strcmp(command_table[0],"history")==0){
      historico = history_list();
      state = history_get_history_state();

      for(i=0;i<state->length;i++) printf("%d - %s \n",i,historico[i]->line);
    }
    else if(strcmp(command_table[0],"exit")==0){
      historico = history_list();
      state = history_get_history_state();
      
      for(i=0;i<FIRST_BUFFER;i++) free(command_table[i]);  
      free(command_table);

      for(i=0;i<state->length;i++){
        free(historico[i]->line);
        free(historico[i]->timestamp);
      }
      free(historico);
      free(state);
      break;
    }
    else {

      if (fork()!=0) {
        /*codigo do pai*/
        waitpid(-1, &status, 0);
      } else {
        /*codigo do filho */
        execve(command_table[0], command_table,0);
      }
    }
  }


  return 0;
}

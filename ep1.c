#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <string.h>
#include <pthread.h>

#define INF 99999999

pthread_mutex_t *mutex;
pthread_cond_t *suspCond;
struct args {
	char *nome;
	double dt;
	double arrive;
	int id;
};
FILE *output;
double scheduler_start,*remaining_times,actual_remaining_time;
int *remaining_threads,n,*suspended,actual_thread;


struct threadInLine {
	int thread;
	double rem_time;
	struct threadInLine *next;
};
typedef struct threadInLine *threadQueue;

threadQueue putInQueue (int thread,double rem_time,threadQueue tq){
	if(tq!=NULL) printf("tq %d %lf\n",thread,rem_time);
	if(tq==NULL || (tq->rem_time > rem_time)){
		threadQueue newQueue = malloc(sizeof(struct threadInLine));
		newQueue->next = tq;
		newQueue -> rem_time =rem_time;
		newQueue -> thread = thread;
		return newQueue;
	}
	tq->next = putInQueue(thread,rem_time,tq->next);
	return tq;
}

int dequeueAndRun(threadQueue tq){ //Retorna numero do processo a ser resumido ou iniciado
	int thread;
	threadQueue toBeFreed;
	if(tq==NULL){
		printf("FUDEU.\n");
		exit(1);
	}
	thread = tq->thread;
	toBeFreed = tq;
	tq=tq->next;
	free(toBeFreed);
	return thread;
}

void printLine (char *nome, double tf, double tr){
  fprintf(output,"%s %lf %lf\n",nome,tf,tr);
}

void printSuzaninha(void *a){
	struct args *th = (struct args *) a;
	int i;
	double begin,rem_time,thread_end;
	struct timeval timestamp;

	rem_time = (th->dt)*1000;

	pthread_mutex_init(&mutex[actual_thread],NULL);
  	pthread_cond_init(&suspCond[actual_thread],NULL);	
	
	while(rem_time>0){
		pthread_mutex_lock(&mutex[actual_thread]);
		while(suspended[actual_thread]) {
			printf("SUSPENDED\n");
			pthread_cond_wait(&suspCond[actual_thread],&mutex[actual_thread]);
		}


		gettimeofday(&timestamp,NULL);
		begin = (double)(timestamp.tv_sec*1000 + timestamp.tv_usec/1000);
		for (i=0;i<1000;i++){
			printf("Per amore hai mai fatto niente solo \nPer amore hai sfidato il vento e urlato mai?\n");
			printf("global %lf\n",actual_remaining_time);
		}
		
		gettimeofday(&timestamp,NULL);
	 	rem_time -= ((double)(timestamp.tv_sec*1000 + timestamp.tv_usec/1000)-begin);
	 	actual_remaining_time=rem_time;
	 	//printf("global %lf\n",actual_remaining_time);
	 	//printf("local %lf\n",rem_time);
		pthread_mutex_unlock(&mutex[actual_thread]);
	}

	gettimeofday(&timestamp,NULL);
	thread_end = (double)timestamp.tv_sec-scheduler_start;
	actual_thread=INF; //vai retornar ao estado de nenhuma thread
	actual_remaining_time=INF; //vai retornar ao estado de nenhuma thread

	printLine(th->nome,thread_end,thread_end-(th->arrive));
	return NULL;
}

void readTraceFile(FILE *trace,int n,double *arrives,double *duration, double *deadline,char **nome){
	char buffer[200],*arg;
	int i=0;

	while(fgets(buffer,200,trace)!=NULL){
		arg = strtok(buffer," \n");
	    arrives[i]=strtof(arg,NULL);
	    arg = strtok(NULL," \n");
	    strcpy(nome[i],arg);
	    arg = strtok(NULL," \n");
	    duration[i]=strtof(arg,NULL);
	    arg = strtok(NULL," \n");
	    deadline[i]=strtof(arg,NULL);
	    i++;		
	}
}

int numberOfLines(FILE *file){
	int ret=1;
	while(!feof(file)){
		if(fgetc(file)=='\n') ret++;
	}
	return ret;
}

void fcfs(double *arrives,double *duration, double *deadline,char **nome){
	pthread_t tid;
	int i;
	struct timeval timestamp;

	gettimeofday(&timestamp,NULL);
	scheduler_start=(double)timestamp.tv_sec;

	//Assumindo que os processos ja estejam ordenados por tempo de chegada.
	for (i=0;i<n;i++){
		struct args a;
		a.arrive=arrives[i];
		a.dt = duration[i];
		a.nome = nome[i];
		actual_thread = i;
		if(pthread_create(&tid,NULL,printSuzaninha,(void *) &a)){
			printf("Erro criando processo %d\n",i);
			exit(1);
		}
		if(pthread_join(tid, NULL)){
			printf("Erro no retorno do processo %d\n",i);
			exit(2);
		}
	}

}

void srtn(double *arrives,double *duration, double *deadline,char **nome){
	int j=0,change=0;
	threadQueue fila=NULL;
	actual_remaining_time=INF;
	actual_thread=INF;
	struct timeval timestamp;
	struct args a;
	pthread_t *tid;
	tid = calloc(n,sizeof(pthread_t));

	gettimeofday(&timestamp,NULL);
	scheduler_start = (double)timestamp.tv_sec;

	while(j<n){
		while(arrives[j]<=((double)timestamp.tv_sec-scheduler_start)){
			if(duration[j]*1000<actual_remaining_time) //Uma thread sera interrompida.
				change=1;
			fila=putInQueue (j, duration[j]*1000,fila);//sendo ou nao interrompida, enfileira
			printf("J: %d\n",j );
			j++;

		}
		if(change){
			change=0;
			printf("Change environment %d\n",actual_thread);

			if(actual_thread!=INF){ //Ha uma thread rodando que sera interrompida
				pthread_mutex_lock(&mutex[actual_thread]);
				suspended[actual_thread] = 1;
				fila=putInQueue(actual_thread,actual_remaining_time,fila);
				pthread_mutex_unlock(&mutex[actual_thread]);
			}


			actual_thread = dequeueAndRun(fila);			
			a.nome = nome[actual_thread];
			a.dt = duration[actual_thread];
			a.arrive = arrives[actual_thread];
			printf("Pass\n");
			printf("ID %d\n",actual_thread);
			printf("NOME %s\n",nome[actual_thread] );

			if(tid[actual_thread]==0) //Primeira vez da thread
				if(pthread_create(&tid[actual_thread],NULL,printSuzaninha,(void *) &a)){
					printf("Ooops!Erro criando novo processo\n");
					exit(1);
				}
			else{ //Reassumindo thread
				pthread_mutex_lock(&mutex[actual_thread]);
				suspended[actual_thread] = 0;
				pthread_cond_signal(&suspCond[actual_thread]);
				pthread_mutex_unlock(&mutex[actual_thread]);
			}
		}		

		gettimeofday(&timestamp,NULL);
	}

}


int main (int argc, char **argv){
  FILE *trace;
  int scalonator=atoi(argv[1]),i;
  double *arrive,*duration,*deadline;
  char **nome;  
  trace = fopen(argv[2],"r");
  output = fopen (argv[3], "w");

  n = numberOfLines(trace);
  trace = fopen(argv[2],"r");



  nome = malloc(n*sizeof(char*));
  for(i=0;i<n;i++) nome[i]=malloc(100*sizeof(char));
  arrive = malloc(n*sizeof(double));
  duration=malloc(n*sizeof(double));
  deadline=malloc(n*sizeof(double));
  remaining_times=malloc((n+1)*sizeof(double));
  remaining_threads=malloc((n+1)*sizeof(int));
  for(i=0;i<n;i++) remaining_times[i]=(double)INF;
  mutex = malloc(n*sizeof(pthread_mutex_t));
  suspCond=malloc(n*sizeof(pthread_cond_t));
  suspended=calloc(n,sizeof(int));


  readTraceFile(trace,n,arrive,duration,deadline,nome);  

  switch(scalonator){
  	case 1:
  		fcfs(arrive,duration,deadline,nome);
  		break;
  	case 2:
  		srtn(arrive,duration,deadline,nome);
  		break;
  	default:
  		printf("Escalonador inexistente.\n");
  }
  fclose(trace);
  fclose(output);

  return 0;
}
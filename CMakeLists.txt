cmake_minimum_required(VERSION 2.8)
project( ep1sh )

include_directories(/usr/include/readline)
SET(FLAGS "-Wall -O3 -g")
SET( CMAKE_C_FLAGS  "${CMAKE_C_FLAGS} ${FLAGS}" )

add_executable( ep1sh ep1so.c )
add_executable( ep1 ep1.c)
target_link_libraries(ep1sh readline)
target_link_libraries(ep1 pthread)

